import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryClient, CategoryDTO } from 'src/app/client/client';


export class BaseComponent<T>{

  items: T[] = [];
  selectedItem:T;
  clonedItem: { [s: number]: T; } = {};
  constructor(protected client: any) { }

  onEditInit(event) {
    this.clonedItem[event.data.id] = { ...event.data };
    console.log(event.data);
  }

  onEditComplete(event) {
    this.clonedItem[event.data.id];
    console.log(event.data);
    this.client.put(event.data).subscribe();
  }
  getData(id) {
    this.client.getList(id).subscribe(data => {
      this.items = data;
    });
  }
  new() {
    this.selectedItem = {} as any;
    this.client.post(this.selectedItem).subscribe((data) => {
      this.items.unshift((this.selectedItem = data));      
      this.items = [...this.items];      
    });
  }
}


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent extends BaseComponent<CategoryDTO> implements OnInit {

  constructor(protected client: CategoryClient, protected router: Router) {
    super(client);
  }

  ngOnInit(): void {
    this.getData(0);
  }

  goTo(id) {
    this.router.navigate(['dashboards', 'question', id]);
  }
}
