import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryClient, CategoryDTO, QuestionClient } from 'src/app/client/client';

@Component({
  selector: 'app-question-create',
  templateUrl: './question-create.component.html',
  styleUrls: ['./question-create.component.scss']
})
export class QuestionCreateComponent implements OnInit {

  categoryId: number;
  selectedCategory: CategoryDTO;

  constructor(protected client: QuestionClient, protected categoryClient: CategoryClient, protected activateRoute: ActivatedRoute) {
    this.categoryId = Number.parseInt(activateRoute.snapshot.params['id']);
  }

  ngOnInit(): void {
    this.categoryClient.getById(this.categoryId).subscribe(data => this.selectedCategory = data);
  }

}
