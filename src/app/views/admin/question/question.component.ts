import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionClient, QuestionDTO } from 'src/app/client/client';
import { BaseComponent } from '../category/category.component';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent extends BaseComponent<QuestionDTO> implements OnInit {
  categoryId: number;

  constructor(protected client: QuestionClient, protected activateRoute: ActivatedRoute, protected router: Router) {
    super(client);
    this.categoryId = Number.parseInt(activateRoute.snapshot.params['id']);
  }

  ngOnInit(): void {
    this.getData(this.categoryId);
  }

  new(): void {
    this.router.navigate(['dashboards', 'question-create', this.categoryId]);
  }

}
